#http://api.etherscan.io/api?module=account&action=txlist&address=0xb5E5585D0057501c91c48094029a6F4FB10B5A01&startblock=0&endblock=99999999&sort=asc&apikey=YourApiKeyToken
from collections import OrderedDict
import requests
from datetime import datetime
from plot import plot_using_plotly
from web3 import Web3, HTTPProvider, IPCProvider
web3 = Web3(HTTPProvider('https://mainnet.infura.io:8545'))
holders = {}
holders_daily = {}
wei = 1000000000000000000.0
Watch_addr = "0xb5E5585D0057501c91c48094029a6F4FB10B5A01"    #Radien Network 0xb5E5585D0057501c91c48094029a6F4FB10B5A01
Watch_addr2 = "0x22b84d5ffea8b801c0422afe752377a64aa738c2"

investor_type = {
"0x25bea2156246ca97a8352e1cbe4b19dd34bb307f":"Flipping(big whale): Gensis Guy",
"0xe767c46329e37c7197c0c5b9200185e6a66b67f2":"Flipping(big whale): LINK, ENG, MDA, TNT, CVC, OMG",
"0x198ef1ec325a96cc354c7266a038be8b5c558f67":"Buy and Hold(big whale)",
"0x3078f22015436d621062f7cc8334774eb5685e97":"Flipping(Small whale): SNT, Argon, ADX, QTUM, LINK",
"0xc8f387eb8edac64efae018dfd10036bdbc059369":"Flipping+hold",
"0x96f0aa4251eb879290d36ae975c57a59f2a5472f":"Flipping",
"0x72b16dc0e5f85aa4bbfce81687ccc9d6871c2965":"Buy and Hold",
"0x3477f07b252ccef3bf1945328dcf5381c28f2d44":"Only Loopring",
"0x0019142ea15d5e963a4b75ac72a6f64b344a8abb":"Buy and Hold",
"0x2c840d94dcf091b87fd63db7eb0885d9ca4b5f79":"Flipping",
"0x2681e0ee432c53a1d2fff0636363a90abac97832":"Buy and Hold",
"0x22b84d5ffea8b801c0422afe752377a64aa738c2":"Buy and Hold(Big whale)"
}

def get_investor_type(addr):
    if investor_type.get(addr):              #if dict is not empty
        return investor_type[addr]
    else:
        return "N/A"


def addHolder(addr,value,Tx):          #Add holders to dict
    if holders.get(addr):              #if dict is not empty
        old_value,Tx = holders[addr]
        value += old_value
        holders[addr] = (value,Tx)
    else:
        holders[addr] = (value,Tx)

def add_to_holders_daily(addr,value,timestamp):
    if addr not in holders_daily:
        holders_daily[addr] = {timestamp:value}
    else:
        holders_daily[addr][timestamp] = value

def holder_daily_to_plot_trace_daily(account):
    holder_daily = holders_daily[account]
    trace = {"name":account}
    for date in holder_daily:
        amount = holder_daily[date]
        if "x" not in trace:
            trace["x"] = [date]
            trace["y"] = [amount]
        else:
            trace["x"].append(date)
            trace["y"].append(amount)
    return trace

def holder_daily_to_plot_trace_total(account):
    holder_daily = holders_daily[account]
    trace = {"name":account}
    total_so_far = 0
    for date in holder_daily:
        amount = holder_daily[date]
        if "x" not in trace:
            trace["x"] = [date]
            trace["y"] = [amount]
            total_so_far = amount
        else:
            trace["x"].append(date)
            total_so_far += amount
            trace["y"].append(total_so_far)
    return trace

def main():
    print("----------Top RDN Investor----------")
    url = "http://api.etherscan.io/api?module=account&action=txlist&address=" + Watch_addr + "&startblock=0&endblock=99999999&sort=asc&apikey=YourApiKeyToken"
    response = requests.get(url)
    data = response.json()

    for r in data['result']:
        holder = r['from'].lower()
        Tx = r['hash']
        value = float(r['value'])/wei
        if(r['isError'] == "0"):
            addHolder(holder,value,Tx)
            date = datetime.fromtimestamp(int(r["timeStamp"]))
            add_to_holders_daily(holder,value,date)

    # Sort holders by their ETH contribution
    sorted_holder = OrderedDict(sorted(holders.items(), key=lambda x: x[1][0],reverse=True))

    # Array that store the trace for plotting
    daily_traces = []
    total_traces = []

    i = 0

    for k,v in sorted_holder.items():
        if("0x22b84d5" in k):
            print (k)
            return

    for k,v in sorted_holder.items():
        if (k == "genesis"):
            bal = 11900000
        else:
            bal = round(web3.eth.getBalance(k)/wei,1)
        if (v[0] > 800 and bal > 2000):

            print("#{}   {}ETH  ---  {} ({}) --- {}".format(i,round(v[0],1),k,bal,get_investor_type(k)))

            i += 1

            daily_trace = holder_daily_to_plot_trace_daily(k)
            daily_traces.append(daily_trace)

            total_trace = holder_daily_to_plot_trace_total(k)
            total_traces.append(total_trace)

    plot_using_plotly("RDN Top Investor daily contribution",daily_traces)
    plot_using_plotly("RDN Top Investor total contribution",total_traces)

main()


# k = 0
# url = "http://api.etherscan.io/api?module=account&action=txlist&address=" + Watch_addr2 + "&startblock=0&endblock=99999999&sort=asc&apikey=YourApiKeyToken"
# response = requests.get(url)
# data = response.json()
#
# for r in data['result']:     #1st depth
#     k += 1
#     print ("#{}".format(k))
#     if(r['to'] == Watch_addr):
#         print ("#{}  {}".format(k,r['hash']))
#
#     url = "http://api.etherscan.io/api?module=account&action=txlist&address=" + r['to'] + "&startblock=0&endblock=99999999&sort=asc&apikey=YourApiKeyToken"
#     response = requests.get(url)
#     data = response.json()
#     for r in data['result']:     #2st depth
#         if(r['to'] == Watch_addr):
#             print (r['hash'])





#OMG transfer

# https://api.etherscan.io/api?module=logs&action=getLogs&fromBlock=4410041&toBlock=latest&
# address=0xd26114cd6ee289accf82350c8d8487fedb8a0c07&
# topic0=0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef

#etherdelta trade

# https://api.etherscan.io/api?module=logs&action=getLogs&fromBlock=4410441&toBlock=latest&
# address=0x8d12a197cb00d4747a1fe03395095ce2a5cc6819&
# topic0=0x6effdda786735d5033bfad5f53e5131abcced9e52be6c507b62d639685fbed6d
