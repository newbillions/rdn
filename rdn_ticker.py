import requests
import time

current_time = int(time.time())

url= "https://token.raiden.network/status.json?ts={}".format(current_time)
response = requests.get(url)
data = response.json()
status = data["status"]
print(status)
